package sf

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
)

type ReturnedFiles struct {
	fn string
	t  time.Time
}

// Creates a stamped file name given the prefix and suffix based on current time.
// EXAMPLE:
//	GetStampedFileName("DATA_", ".BAK") returns "DATA_YYYYMMDD_hhmmss.BAK",
//		where YYYYMMDD_hhmmss is current time.
// ARGUMENTS:
//	prefix: what you want to have in front of YYYYMMDD_hhmmss time stamp
//	suffix: what you want to have after the YYYYMMDD_hhmmss time stamp
func GetStampedFileName(prefix, suffix string) string {
	t := time.Now()
	y := fmt.Sprintf("%04d", t.Year())
	m := fmt.Sprintf("%02d", t.Month())
	d := fmt.Sprintf("%02d", t.Day())
	hh := fmt.Sprintf("%02d", t.Hour())
	mm := fmt.Sprintf("%02d", t.Minute())
	ss := fmt.Sprintf("%02d", t.Second())
	timestamp := y + m + d + "_" + hh + mm + ss
	return prefix + timestamp + suffix
}

// Returns timestamp from given file name by stripping prefix and suffix.
// EXAMPLE:
//	GetStampedFileNameTime("DATA_", ".BAK", "DATA_20210816_104620.BAK") returns
//		16th Aug 2021, 10:46:20
//	GetStampedFileNameTime("DATA_", ".BAK", "DATA_20210816_10420.BAK") returns
//		error (invalid time stamp)
//	GetStampedFileNameTime("DATA_", ".BAK", "DATA20210816_104620.BAK") returns
//		error (file does not match the prefix and/or suffix)
func GetStampedFileNameTime(prefix, suffix, fn string) (time.Time, error) {
	t := time.Now()
	stamp := strings.TrimPrefix(strings.TrimSuffix(fn, suffix), prefix)
	if (stamp[8] != '_') || (len(stamp) != 15) {
		e := fmt.Errorf("Not a timestamp")
		return time.Time{}, e
	}

	y, err := strconv.Atoi(stamp[:4])
	if err != nil {
		return time.Time{}, err
	}

	m, err := strconv.Atoi(stamp[4:6])
	if err != nil {
		return time.Time{}, err
	}

	d, err := strconv.Atoi(stamp[6:8])
	if err != nil {
		return time.Time{}, err
	}

	hh, err := strconv.Atoi(stamp[9:11])
	if err != nil {
		return time.Time{}, err
	}

	mm, err := strconv.Atoi(stamp[11:13])
	if err != nil {
		return time.Time{}, err
	}

	ss, err := strconv.Atoi(stamp[13:15])
	if err != nil {
		return time.Time{}, err
	}

	return time.Date(y, time.Month(m), d, hh, mm, ss, 0, t.Location()), nil
}

// Returns a slice of file names in a given path according to a pattern.
// IMPORTANT NOTE: the returned slice is ordered and returns last files first!
// EXAMPLE:
//	Ls("./test_data", "DATA_*.BAK") returns all files starting with "DATA_"
//		and ending with ".BAK" in a directory path
func Ls(path, prefix, suffix string) ([]string, error) {
	var returned []string
	files, err := ioutil.ReadDir(path)
	pattern := prefix + "*" + suffix
	if err != nil {
		return nil, err
	}
	for _, f := range files {
		if !f.IsDir() {
			matched, err := filepath.Match(pattern, f.Name())
			if err != nil {
				return nil, err
			}
			if matched {
				returned = append(returned, f.Name())
			}
		}
	}
	sort.Slice(returned, func(i, j int) bool {
		return returned[i] > returned[j]
	})
	return returned, nil
}

// Creates a file if does not exist, or changes its time to current time if it does,
// just like the unix touch command would do.
// EXAMPLE:
//	Touch("test.txt") touches the file test.txt or creates one if it does not exist
func Touch(fileName string) error {
	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		file, err := os.Create(fileName)
		if err != nil {
			return err
		}
		defer file.Close()
	} else {
		currentTime := time.Now().Local()
		err = os.Chtimes(fileName, currentTime, currentTime)
		if err != nil {
			return err
		}
	}
	return nil
}

// Creates count of sample stamped files in folder filePath that are 2 seconds apart.
func CreateTestData(filePath string, count int) error {
	for i := 1; i <= count; i++ {
		err := Touch(path.Join(filePath, GetStampedFileName("DATA_", ".BAK")))
		if err != nil {
			return err
		}
		time.Sleep(2 * time.Second)
	}
	return nil
}

// Returns last count files in a filePath according to a pattern. If count is > than number
// of files, it returns all files.
// Usefull for getting the list of current backup files to transfer to offsite location
// EXAMPLE:
//	LsLast("./test_data", "DATA", ".BAK", 5) returns last 5 files
func LsLast(filePath, prefix, suffix string, count int) ([]string, error) {
	var returned []string
	returned, err := Ls(filePath, prefix, suffix)
	if err != nil {
		return returned, err
	}
	if count > len(returned) {
		return returned, nil
	}
	returned = returned[0:count]
	return returned, nil
}

// Returns all files excluding the last count files in a filePath according to a pattern.
// If count is > than number of files, it returns no files.
// Usefull for getting the list of old files to delete.
// EXAMPLE:
//	LsRest("./test_data", "DATA_", ".BAK", 5) returns all but last 5 files
func LsRest(filePath, prefix, suffix string, count int) ([]string, error) {
	var returned []string
	returned, err := Ls(filePath, prefix, suffix)
	if err != nil {
		return returned, err
	}
	if count > len(returned) {
		return nil, nil
	}
	returned = returned[count:]
	return returned, nil
}

// Returns all files older than cutoff date.
// Usefull for getting the list of old files to delete.
// EXAMPLE:
//	LsOlder("./test_data", "DATA_", ".BAK", time.Now().Add(-24*time.Hour)) returns all files older than 1 day
func LsOlder(filePath, prefix, suffix string, cutoff time.Time) ([]string, error) {
	var returned []string
	var rf ReturnedFiles
	returned, err := Ls(filePath, prefix, suffix)
	if err != nil {
		return returned, err
	}
	//fmt.Println("cutoff:", cutoff)

	for i, f := range returned {
		rf.fn = f
		rf.t, err = GetStampedFileNameTime(prefix, suffix, f)
		//fmt.Println("fn:", rf.fn, "t:", rf.t)
		if err != nil {
			return nil, err
		}
		if cutoff.After(rf.t) {
			//fmt.Println("fn:", rf.fn, "ts:", rf.t)
			return returned[i:], nil
		}
	}

	return nil, nil
}

// Returns all files newer than cutoff date.
// Usefull for getting the list of current files to transfer to offsite location.
// EXAMPLE:
//	LsNewer("./test_data", "DATA_", ".BAK", time.Now().Add(-24*time.Hour)) returns all files newer than 1 day
func LsNewer(filePath, prefix, suffix string, cutoff time.Time) ([]string, error) {
	var returned []string
	var rf ReturnedFiles
	returned, err := Ls(filePath, prefix, suffix)
	if err != nil {
		return returned, err
	}
	//	fmt.Println("cutoff:", cutoff)

	for i, f := range returned {
		rf.fn = f
		rf.t, err = GetStampedFileNameTime(prefix, suffix, f)
		if err != nil {
			return nil, err
		}
		if cutoff.After(rf.t) {
			//			fmt.Println("fn:", rf.fn, "ts:", rf.t)
			return returned[0:i], nil
		}
	}

	return returned, nil
}

// Returns all files newer than cutoff date. If there is not enough files returned (< count), it returns
// last count files.
// Usefull for transfering at least count files to offsite destination.
// EXAMPLE:
//	LsNewerOrCount("./test_data", "DATA_", ".BAK", time.Now().Add(-24*time.Hour), 4) returns
//		all files timestamped within last 24 hours but it their count is less than 4, it returns
//		last 4 files whatever their timestamps are.
func LsNewerOrCount(filePath, prefix, suffix string, cutoff time.Time, count int) ([]string, error) {
	returned, err := LsNewer(filePath, prefix, suffix, cutoff)
	if err != nil {
		return returned, err
	}
	if len(returned) < count {
		return LsLast(filePath, prefix, suffix, count)
	}
	return returned, nil
}

// Returns all files older than cutoff date. But it counts with which files should be KEPT, so if it should
// return too many files (for example to deletion) that there would left LESS than count of files it returns
// only so many files, that if you'd remove them, there will still be at least count files in the directory
// regardless of their time stamps.
// Usefull for deleting older files but keep enough backup copies.
// EXAMPLE:
//	LsOlderOrCount("./test_data", "DATA_", ".BAK", time.Now().Add(-24*time.Hour), 4) returns
//		all files older than 24 hours but keeps (accounts for) at least 4 files.
func LsOlderOrCount(filePath, prefix, suffix string, cutoff time.Time, count int) ([]string, error) {
	returned, err := LsOlder(filePath, prefix, suffix, cutoff)
	if err != nil {
		return returned, err
	}

	all, err := Ls(filePath, prefix, suffix)
	if err != nil {
		return all, err
	}

	if (len(all) - count) < len(returned) {
		return LsRest(filePath, prefix, suffix, count)
	}
	return returned, nil
}
