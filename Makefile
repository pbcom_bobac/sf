sf:
	GOOS=darwin GOARCH=amd64 go build  -o bin/sf-darwin_x64 ./cmd/sf
	GOOS=linux GOARCH=amd64 go build  -o bin/sf-linux_x64 ./cmd/sf
	GOOS=linux GOARCH=arm go build  -o bin/sf-linux_arm ./cmd/sf
	GOOS=linux GOARCH=arm64 go build  -o bin/sf-linux_arm64 ./cmd/sf
	GOOS=windows GOARCH=amd64 go build  -o bin/sf-win_x64.exe ./cmd/sf
	GOOS=windows GOARCH=386 go build  -o bin/sf-win_x86.exe ./cmd/sf