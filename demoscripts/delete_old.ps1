﻿$old = .\sf.exe older .\test_data -p DATA_ -s .BAK -d 1 -m 3
foreach($fn in $old) {
    Write-Host "Deleting $fn"
    Remove-Item ".\test_data\$fn" -Whatif
}
