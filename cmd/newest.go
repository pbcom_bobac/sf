/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/pbcom_bobac/sf"
)

// newestCmd represents the newest command
var newestCmd = &cobra.Command{
	Use:   "newest",
	Short: "Returns N newest files",
	Long: `Returns N newest files. Example:

sf newest --prefix DATA_ --suffix .BAK --count 5

might return newest 5 files in the current directory.

sf newest ./test_data -p "DATA_" -s ".BAK" -c 3

might return newest 3 files in the ./test_data directory.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		var dir string
		if len(args) > 0 {
			dir = args[0]
		} else {
			dir = "./"
		}
		files, err := sf.LsLast(dir, Prefix, Suffix, Count)
		if err != nil {
			log.Fatalf("Cannot read directory. Error: %v\n", err)
		}
		for _, f := range files {
			fmt.Println(f)
		}
	},
}

func init() {
	rootCmd.AddCommand(newestCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// newestCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// newestCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	newestCmd.Flags().IntVarP(&Count, "count", "c", 1, "number of files returned")
}
