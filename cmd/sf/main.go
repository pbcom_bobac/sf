package main

import (
	"gitlab.com/pbcom_bobac/sf/cmd"
)

const (
	testDataDir = "./test_data"
)

func main() {
	cmd.Execute()
}
