/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/pbcom_bobac/sf"
)

// olderCmd represents the older command
var olderCmd = &cobra.Command{
	Use:   "older",
	Short: "Returns list of files older than N days",
	Long: `Returns list of files older than N days. Example:
	
sf older --prefix DATA_ --suffix .BAK --days 5
		
might return all files older than 5 days in the current directory
		
sf older ./test_data -p "DATA_" -s ".BAK" -d 3
		
might return all files older than 3 days in the ./test_data directory.

sf older ./test_data -p "DATA_" -s ".BAK" -d 3 -m 5

might return all filer older than 3 days in the ./test_data directory, but in case
that there are not enough newer files, it will not include 5 most recent files.

This is usefull, when you e.g. want to delete old backups but need to be sure,
that there are at least some left, even if they are not recent.
`,
	Run: func(cmd *cobra.Command, args []string) {
		var dir string
		var files []string
		var err error

		if len(args) > 0 {
			dir = args[0]
		} else {
			dir = "./"
		}
		Hrs = -24 * time.Duration(Days) * time.Hour
		if MinCount == 0 {
			files, err = sf.LsOlder(dir, Prefix, Suffix, time.Now().Add(Hrs))
		} else {
			files, err = sf.LsOlderOrCount(dir, Prefix, Suffix, time.Now().Add(Hrs), MinCount)
		}
		if err != nil {
			log.Fatalf("Cannot read directory. Error: %v\n", err)
		}
		for _, f := range files {
			fmt.Println(f)
		}
	},
}

func init() {
	rootCmd.AddCommand(olderCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// olderCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// olderCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	olderCmd.Flags().IntVarP(&Days, "days", "d", 7, "older than N days")
	olderCmd.Flags().IntVarP(&MinCount, "min", "m", 0, "always keep at least N files (regardless their age)")
}
