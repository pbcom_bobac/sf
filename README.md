# SF - Stamped Filename 1.0.1
A utility to work with files that are time-stamped. Example of such file name is `DATA_20210818_094812.BAK`. In general, it consists of a `PREFIX`, time stamp in form of `YYYYDDMM_hhmmss` and the `SUFFIX`. In revious example the `DATA_` is the PREFIX, `20210818_094812` is the timestamp and `.BAK` is the SUFFIX.

## What it can do?
It will help you with following tasks:
1. Generate a file name based on described scheme and derive the name from current system time.
2. Get a list of filenames in current or given directory that fulfills various conditions based on their age

You can get a list of files that:
1. Are newer than N days.
2. Are older than N days.
3. Are newer than N days but always return at least M files, if there is not enough newer files.
4. Are older than N days but keep in mind that there should be at least M newest files there were not returned.
5. Are simple the N newest.
6. Are simply the N oldest.

### Why do I need those functions?
This function set will help you design varions backup or data transfer scripts, or housekeeping procedures that perform task such as:
1. Do a backup of a database or particular application.
2. Delete all files older than N days but never delete last M files in case that the backup jobs ceased to work.
3. Transfer newest files to offsite location

### What is the output of the utility?
The output is always a text printed in standard output, no, one or multiple filenames adhering to the given conditions. The aim is to use that output as an input of your scripts. SF itself does not do any file manipulation nor copying, it does not generate any database dumps. But will help you to pass a unique file name to your dump script or a list of files that your script should process.

### Why such a ridiculous naming scheme?
Is it not enough to have a time stamps that are assigned to each file at the time of creation or modification by the OS?

The aim here is to provide two things:
1. User should be able to see at fist glance on the file name, how old it actually is. It is even easy to sort the files just by names and receive the time-sorted list.
2. Avoid some corner scenarios, when touching older files could lead to messing up with the backup aging process. One has to deliberately rename a file to make it "newer" or "older".

## Examples

### Bash
Backup a MySQL databases:
```
#!/bin/bash
mysqldump --all-databases --single-transaction --quick --lock-tables=false > data-backup/$(sf get -p mysql_ -s .sql) -u backupuser -p password
```

Delete all backups older than 30 days but keep at least 3 files:
```
#!/bin/bash
sf older backups/ -p DATA_ -s .BAK -d 1 -m 3 | awk '{print "backups/" $0}' | xargs rm
```

Copy all backups from previous day to a different directory:
```
#!/bin/bash
sf newer backups/ -p DATA_ -s .BAK -d 1 | xargs -I % cp backups/% tmp/%
```

### PowerShell
Backup a MSSQL database:
```
$fn = .\sf.exe get -p TestDB_ -s .BAK
Write-Host "Backing up to file: $fn"
Write-Host "BACKUP DATABASE TestDB TO DISK='.\$fn' WITH FORMAT"
& 'C:\Program Files\Microsoft SQL Server\150\Tools\Binn\OSQL.EXE' -E -Q "BACKUP DATABASE TestDB TO DISK='.\$fn' WITH FORMAT"
```

Delete all backups older than 30 days but keep at least 3 files:
```
$old = .\sf.exe older .\test_data -p DATA_ -s .BAK -d 30 -m 3
foreach($fn in $old) {
    Write-Host "Deleting $fn"
    Remove-Item ".\test_data\$fn"
}
```

Copy all backups from previous day to a different directory:
```
$new = .\sf.exe newer .\test_data -p DATA_ -s .BAK -d 1
foreach($fn in $new) {
    Write-Host "Copying $fn"
    Copy-Item ".\test_data\$fn" ".\output_data\$fn"
}
```

## Building from source
You need a Go version 1.16.3 or later for your platform. Tested on Windows 10 20H2, Mac OS X  11.5.1, expected to work on Linux :-) Probably would work with any 1.16 line, but did not tested that. Older versions of Go would probably have issues with modules.

```
git clone https://gitlab.com/pbcom_bobac/sf.git
cd sf
go build ./cmd/sf
```

## Installation
Put either `sf.exe` (Windows) or `sf` (*nix) anywhere to your PATH.